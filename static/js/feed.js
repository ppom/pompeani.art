document.addEventListener("DOMContentLoaded", function() {
  console.log('feed.js loaded');
  savvior.init('#savviorGrid', {
    "screen and (min-width: 700px)": { columns: 3 },
    "screen and (max-width: 700px)": { columns: 2 },
    "screen and (max-width: 350px)": { columns: 1 },
  });
});
