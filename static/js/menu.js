
document.addEventListener("DOMContentLoaded", function() {
  console.log('menu.js loaded');
  const button = document.getElementById('menu-button');
  button.onclick = function() {
    const header = document.getElementsByTagName('header')[0];
    header.classList.toggle('opened');
  };
});
