# pompeani.art

Ce site est disponible à l'adresse suivante : https://pompeani.art

Le code du site est disponible à l'adresse suivante : https://framagit.org/ppom/pompeani.art

Ce site a été codé par [ppom](https://framagit.org/ppom) et designé par Sébastien Jenger de la société [Primo&Primo Creative Agency](https://primoetprimo.com/)

## Licenses

Le code est sous la licence Blue Oak 1.0.0.

Les images sont sous la licence Creative Commons Attribution-NonCommercial-NoDerivatives 4.0.

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">
  <img alt="License Creative Commons Attribution-NonCommercial-NoDerivatives 4.0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" />
</a>

## Hébergement

Le site est hébergé sur un VPS de la société OVH, dans un datacenter à Francfort, en Allemagne.

## Vie privée

Le site ne fait pas usage de cookies et ne collecte pas de données personnelles.

Le logiciel de statistiques [Plausible](https://plausible.io) est utilisé, garantissant l'anonymat des visites.

## Usage

Le générateur de site statique utilisé est [Zola](https://www.getzola.org).
